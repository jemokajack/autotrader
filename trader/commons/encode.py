import tensorflow as tf

class Autoencoder(object):
    def __init__(self, inputDim, encodedDim, learningRate=0.01):
        self.inputDim = inputDim
        self.encodedDim = encodedDim
        self.x = tf.placeholder(tf.float32, [None, self.inputDim])
        
        w1 = tf.Variable(tf.zeros([self.inputDim, self.encodedDim]))
        b1 = tf.Variable(tf.constant(0.1, shape=[self.encodedDim]))  
        self.encoded = tf.matmul(self.x, w1)+b1
        
        w2 = tf.Variable(tf.zeros([self.encodedDim, self.inputDim]))
        b2 = tf.Variable(tf.constant(0.1, shape=[self.inputDim])) 
        self.yhat = tf.matmul(self.encoded, w2)+b2
        
        loss = tf.sqrt(tf.reduce_mean(tf.square(self.x-self.yhat)))
        
        self.trainingOperation = tf.train.AdagradOptimizer(learningRate).minimize(loss)
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        
    def train(self, data, epoch=100):
        for _ in range(epoch):
            self.sess.run(self.trainingOperation, feed_dict={self.x: data})
    
    def encode(self, data):
        return self.sess.run(self.encoded, feed_dict={self.x: data})
    
    def decode(self, data):
        return self.sess.run(self.yhat, feed_dict={self.encoded: data}) 
        